/*
* Class from show/hide menu on mobile version
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/

let header = document.body.querySelector("header");
let mobileBtn = header.querySelector(".mobile-menu-call");
let nav = header.querySelector("nav");

export class MobileMenu {
	show() {
		mobileBtn.addEventListener("click", () => {
			nav.className += "active";
		});
	}

	hide() {
		document.addEventListener("click", (e) => {
			if(e.target.localName === "main" || e.target.classList.contains("close-menu")) {
				nav.classList.remove("active");
			}
		});
	}
}
