/*
* Module from ankor animate
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/

export function animateAnkor(position) {
	const curPos = window.pageYOffset || document.body.scrollTop;
	const ratio = 300 / 15;
	const step = (position - curPos) / ratio;
	let count = 0;
	let animateInterval = setInterval(() => {
		count++;
		if(count > ratio) {
			clearInterval(animateInterval);
		} else {
			window.scrollBy(0, step);
		}
	}, 15);
}
