/*
* Class from animation
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/

export class Animate {
	animateOpacity(selector, degres, cb) {
		let finishCheck = 100;
		let i = 0;

		if(degres) {
			i = 100;
			finishCheck = 0;
		}

		const id = setInterval(animate, 3);
		function animate() {
			if(i == finishCheck) {
				clearInterval(id);
				if(cb) {
					cb();
				}
			} else {
				if(degres) {
					i--;
				} else {
					i++;
				}
				selector.style.opacity = i / 100;
			}
		};
	}

	fadeIn(selector, cb) {
		selector.style.display = "block";
		selector.style.opacity = "0";
		this.animateOpacity(selector, false, () => {
			if(cb) {
				cb();
			}
		});
	}

	fadeOut(selector, cb) {
		this.animateOpacity(selector, true, () => {
			selector.style.display = "none";
			if(cb) {
				cb();
			}
		});
	}
}
