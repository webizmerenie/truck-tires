/*
* class from button scroll top
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/

import { animateAnkor } from "./ankor-animate.js";

export class ScrollTop {
	constructor(item) {
		this.topBtn = document.body.querySelector(item);
	}

	visible() {
		const btn = this.topBtn;

		this.scrollHeight = Math.max(
			document.body.scrollHeight, document.documentElement.scrollHeight,
			document.body.offsetHeight, document.documentElement.offsetHeight,
			document.body.clientHeight, document.documentElement.clientHeight
		);

		this.scrolled = window.pageYOffset || document.body.scrollTop;

		if(this.scrollHeight * 0.3 < this.scrolled) {
			btn.style.visibility = "visible";
			btn.style.opacity = 1;
		} else {
			if(btn.style.opacity == 1) {
				btn.style.opacity = 0;
				setTimeout(() => {
					btn.style.visibility = "hidden";
				}, 300);
			}
		}
	}

	scrolling() {
		this.topBtn.addEventListener("click", () => {
			animateAnkor(0);
		});
	}
}
