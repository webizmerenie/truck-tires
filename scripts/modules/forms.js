/*
* Class from form
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/

require("jquery.inputmask");
import { Animate } from "./animation.js";

export class Forms {
	constructor() {
		this.overlay = document.querySelector(".overlay");
		const callMe = document.body.querySelector("#call-me");
		const inpTel = callMe.querySelector("input[name='tel']");
		this.telMask = Inputmask("+7 (999)-999-99-99", { clearMaskOnLostFocus: false }).mask(inpTel);
		this.animateClass = new Animate();
	}

	openWindow(selector) {
		const target = document.querySelector(selector);
		this.animateClass.fadeIn(target);
		this.overlay.style.display = "block";
	}

	closeWindow(selector) {
		this.animateClass.fadeOut(selector, () => {
			const sucsessMsg = selector.querySelector(".sucsess");
			const errorMsg = selector.querySelector(".error-msg");
			sucsessMsg.style.display = "none";
			errorMsg.style.display = "none";
		});
		this.overlay.style.display = "none";
	}

	validation(selector) {
		const parent = selector.parentElement;
		const inpText = selector.querySelectorAll(".required");
		const errorMsg = selector.querySelector(".error-msg");
		let error = false;

		inpText.forEach(input => {
			if(/^\s+$/.test(input.value) || input.value === "") {
				input.classList.add("error");
				error = true;
				if(!errorMsg.offsetParent) {
					this.animateClass.fadeIn(errorMsg);
				}
			} else {
				input.classList.remove("error");
			}
		});

		if(parent.id === "call-me") {
			if(!this.telMask.isComplete()) {
				this.telMask.el.classList.add("error");
				error = true;
			}
		}

		if(!error) {
			const sucsessMsg = parent.querySelector(".sucsess");
//			this.animateClass.fadeIn(sucsessMsg, () => {
//				selector.reset();
//			});
			selector.submit();
		}
	}
}
