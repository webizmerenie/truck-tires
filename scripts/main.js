/*
* Main file scripts
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/
import "./polifils.js";
import { MobileMenu } from './modules/mobile-menu.js';
import { MainPage } from './pages/main.js';
import { ScrollTop } from './modules/scroll-top.js';
import { Contacts } from './pages/contacts.js';
import { animateAnkor } from './modules/ankor-animate.js';
import { Catalog } from './pages/catalog.js';
import { Forms } from './modules/forms.js';
import { Basket } from './pages/basket.js';
import $ from 'jquery';
require('fancybox')($);

document.addEventListener("DOMContentLoaded", function(event) {
	const classMainPage = document.body.querySelector(".content.main-page");
	const contactsPage = document.body.querySelector(".content.contacts");
	const catalogPage = document.body.querySelector(".content.catalog");
	const fancy = document.body.querySelectorAll(".fancy");

	const mobileMenu = new MobileMenu();
	const scrollTop = new ScrollTop(".top");
	const contacts = new Contacts();
	const formClass = new Forms();
	const basketClass = new Basket();

	mobileMenu.show();
	mobileMenu.hide();
	scrollTop.visible();
	scrollTop.scrolling();
	window.onscroll = () => {
		scrollTop.visible();
	}

	if (classMainPage) {
		const mainPage = new MainPage();
		mainPage.sliderInit();
		mainPage.filterFormStyles(".filters .column form .styler");
		mainPage.adventureHoverAnimate(".adventure ul");
		mainPage.caruselSales();
	}

	if (contactsPage) {
		const map = contactsPage.querySelector(".map");
		const imgBaloon = map.querySelector("img");
		contacts.map(map.id, map.dataset.center,  map.dataset.baloon,  map.dataset.baloonText, imgBaloon.src);
	}

	$(fancy).fancybox();

	const tiresPage = document.body.querySelector(".content.tires");
	if(tiresPage) {
		const tiresPageNav = tiresPage.querySelectorAll(".section nav");
		const map = tiresPage.querySelector(".map");
		if(map) {
			const imgBaloon = map.querySelector("img");
			contacts.map(map.id, map.dataset.center,  map.dataset.baloon,  map.dataset.baloonText, imgBaloon.src);
		}

		tiresPageNav.forEach((item) => {
			item.addEventListener("click", (e) => {
				const target = e.target.closest("a");

				if(target) {
					e.preventDefault();
					const corY = tiresPage.querySelector(target.hash).offsetTop;
					animateAnkor(corY);
				}
			});
		});
	}

	if (catalogPage) {
		const catalogClass = new Catalog(".catalog");
		catalogClass.toggleManufactureFilter(".filter .toggle-more");
		catalogClass.eventListenerOnlyNumber(".items .container table");
		catalogClass.eventListenerOnlyNumber("article .item .form-group");
		catalogClass.triggerBasket(".add-basket");
	}

	document.body.addEventListener("click", (e) => {
		const target = e.target;
		const callPopup = e.target.closest(".call-popup");
		const close = e.target.closest(".close");
		const overlay = e.target.closest(".overlay");

		if(callPopup) {
			e.preventDefault();
			formClass.openWindow(callPopup.hash);
		}

		if(close) {
			const parent = close.closest(".popup-windows");
			formClass.closeWindow(parent);
		}

		if(overlay) {
			const target = document.body.querySelectorAll(".popup-windows");
			target.forEach(item => {
				formClass.closeWindow(item);
			});
		}
	});

	document.addEventListener("submit", (e) => {
		e.preventDefault();
		const target = e.target;
		formClass.validation(target);
	});

	basketClass.init();
});
