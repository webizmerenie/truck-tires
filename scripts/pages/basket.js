/*
* Scripts from basket
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/
import { Animate } from "../modules/animation.js";
import { Catalog } from "./catalog.js";

export class Basket {
	constructor() {
		this.main = document.body.querySelector(".basket .items form");
		this.animateClass = new Animate();
		this.catalogClass = new Catalog();
	}

	init() {
		if(this.main) {
			this.table = this.main.querySelector("table");
			this.table.addEventListener("click", (e) => {
				e.preventDefault();
				this.deleteItem(e);
			});

			this.table.addEventListener("keypress", (e) => {
				this.countItem(e);
			});

			this.main.addEventListener("submit", (e) => {
				console.log(e);
			});
		}
	}

	stringToNumber(str) {
		return Number(str.slice(0, -4).replace(" ", ""));
	}

	numberToString(str) {
		return str.toString().replace(' ', '').replace(/^(\d+)(\d{3})$/g, '$1 $2');
	}

	deleteItem(e) {
		const target = e.target;
		if(target.classList.contains("delete")) {
			const tr = target.closest("tr");
			this.animateClass.fadeOut(tr, () => {
				tr.parentElement.removeChild(tr);
				this.countTotal();
				const items = this.table.querySelectorAll("tr");
				if(items.length === 1) {
					window.location.reload();
				}
			});
		}
	}

	countItem(e) {
		this.catalogClass.inputOnlyNumber(e);

		setTimeout(() => {
			const target = e.target;
			const targetVal = Number(target.value);
			const parent = target.closest("tr");

			const basicPrice = parent.querySelector(".basic-price");
			const currentPrice = parent.querySelector(".current-price");
			const basicPriceVal = this.stringToNumber(basicPrice.innerText);
			let res = targetVal * basicPriceVal;
			//res - сумма элемента после изменения количества
			currentPrice.innerText = this.numberToString(res) + " руб.";
			this.countTotal();
		}, 100);
	}

	countTotal() {
		const arrPrice = this.table.querySelectorAll(".current-price");
		const total = this.table.querySelector(".total td:last-of-type");
		let res = 0;

		arrPrice.forEach(item => {
			const priceItem = this.stringToNumber(item.innerText);

			res+= priceItem;
		});
		//res - сумма итого
		total.innerText = this.numberToString(res) + " руб.";
	}
}
