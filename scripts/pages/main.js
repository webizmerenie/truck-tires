/*
* Scripts from main page
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/
import $ from 'jquery';
window.jQuery = $;
import 'slick-carousel';
require('jquery-nice-select');

export class MainPage {
	constructor() {
		this.main = document.body.querySelector(".main-page");
	}

	sliderInit() {
		this.slider = this.main.querySelector(".slider ul");
		$(this.slider).slick({
			dots: true
		});
	}

	filterFormStyles(selector) {
		this.filtersInps = this.main.querySelectorAll(selector);
		if(this.filtersInps) {
			$(this.filtersInps).niceSelect();
		}
	}

	adventureHoverAnimate(selector) {
		this.hoverElement = this.main.querySelector(selector);

		const mouseHandler = (e) => {
			const target = e.target.closest("li");
			const type = e.type;

			if(target) {
				const hidden = target.querySelector(".hidden");
				if(e.type === "mouseover") {
					hidden.style.display = "block";
					hidden.classList.remove("flipOutY");
					hidden.classList.add("flipInY");
				} else {
					hidden.classList.remove("flipInY");
					hidden.classList.add("flipOutY");
				}
			}
		}

		this.hoverElement.addEventListener("mouseover", (e) => {
			mouseHandler(e);
		});

		this.hoverElement.addEventListener("mouseout", (e) => {
			mouseHandler(e);
		});
	}

	caruselSales() {
		this.carusel = this.main.querySelector(".sales ul");
		$(this.carusel).slick({
			arrows: false,
			dots: false,
			infinite: false,
			slidesToShow: 6,
			responsive: [
				{
					breakpoint: 1380,
					settings: {
						slidesToShow: 5
					}
				},
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 4
					}
				},
				{
					breakpoint: 960,
					settings: {
						slidesToShow: 3
					}
				},
				{
					breakpoint: 680,
					settings: {
						slidesToShow: 2
					}
				}
			]
		});
	}
}
