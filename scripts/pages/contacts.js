/*
* Scripts from contacts page
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/

export class Contacts {
	constructor() {
		this.main = document.body.querySelector(".contacts");
	}

	map(idMap, centerMap, centerBaloon, textHover, srcBaloon) {
		this.centerMapArr = centerMap.split(",");
		this.centerBaloonArr = centerBaloon.split(",");

		ymaps.ready(() => {
			const map = new ymaps.Map(idMap, {
				center: this.centerMapArr,
				zoom: 16,
				controls: ["zoomControl", "typeSelector"]
			});

			map.behaviors.disable('scrollZoom');

			const baloon = new ymaps.Placemark(this.centerBaloonArr,
				{
					hintContent: textHover
				},
				{
					iconLayout: 'default#image',
					iconImageHref: srcBaloon,
					iconImageSize: [49, 71],
					iconImageOffset: [-24.5, -71]
			});

			map.geoObjects.add(baloon);
		});
	}
}
