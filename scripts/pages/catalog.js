/*
* Scripts from catalog pages
*
* @author Andrey Chechkin
* @license GNU/AGPLv3
* @see {@link https://www.gnu.org/licenses/agpl-3.0.txt|License}
*/
import { MainPage } from "./main.js";

export class Catalog {
	constructor(selector) {
		if(selector) {
			this.main = document.body.querySelector(selector);
			this.mainPageClass = new MainPage;
			this.mainPageClass.adventureHoverAnimate.call(this, ".adventure ul");
			this.mainPageClass.filterFormStyles.call(this, ".filter form .styler");
		}
	}

	inputOnlyNumber(event) {
		const charCode = (event.which) ? event.which : event.keyCode;
		const target = event.target;
		if(charCode > 31 && (charCode < 48 || charCode > 57) &&
			(event.ctrlKey === false || charCode !== 97)){
			event.preventDefault();
			return false;
		}
	}

	toggleManufactureFilter(selector) {
		const target = this.main.querySelector(selector);
		if(target) {
			target.addEventListener("click", (e) => {
				e.preventDefault();
				const target = e.target;

				this.main.querySelector(target.hash).classList.toggle("show");
				target.classList.toggle("show");

				if(target.classList.contains("show")) {
					target.innerText = "Меньше производителей";
				} else {
					target.innerText = "Больше производителей";
				}
			});
		}
	}

	eventListenerOnlyNumber(selector) {
		const target = this.main.querySelector(selector);
		if(target) {
			target.addEventListener("keypress", (e) => {
				this.inputOnlyNumber(e);
			});
		}
	}

	triggerBasket(selector) {
		this.main.addEventListener("click", e => {
			const target = e.target.closest(selector);
			if(target) {
				e.preventDefault();
				const basket = document.body.querySelector("header .container .column-backet");
				basket.classList.toggle("pulse");

				setTimeout(() => {
					basket.classList.toggle("pulse");
				}, 2500);
			}
		});
	}
}
