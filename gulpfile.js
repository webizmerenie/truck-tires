'use strict';
var gulp = require('gulp');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var clean = require('gulp-clean');
var jade = require('gulp-jade');
var plumber = require('gulp-plumber');
var browserSync = require('browser-sync').create();
var watch = require('gulp-watch');
var browserify = require('gulp-browserify');
var symlink = require('gulp-symlink');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');

var tplPath = './';

gulp.task('styles-clean', function() {
	return gulp.src(tplPath + '/app/*.css', { read: false })
		.pipe(clean());
});

gulp.task('styles', ['styles-clean'], function() {
	gulp.src(tplPath + '/sass/**/*.sass')
		.pipe(plumber())
		.pipe(sass().on('error', sass.logError))
		.pipe(rename('build.css'))
		.pipe(autoprefixer('last 10 versions', '> 1%', 'ie 9'))
		.pipe(gulp.dest(tplPath + '/app/'))
		.pipe(browserSync.stream());
});

gulp.task('jade', function() {
	gulp.src(tplPath + '/jade/*.jade')
		.pipe(plumber())
		.pipe(jade({
			pretty: true
		}))
		.pipe(gulp.dest(tplPath + './app/'))
		.pipe(browserSync.stream());
});

gulp.task('server', function() {
	browserSync.init({
		host: "192.168.1.126",
		server: {
			baseDir: './app'
		}
	});
});

gulp.task('scripts', function() {
	gulp.src(tplPath + 'scripts/main.js')
		.pipe(plumber())
		.pipe(browserify())
		.pipe(rename('build.js'))
		.pipe(gulp.dest(tplPath + '/app/'))
		.pipe(browserSync.stream());
});

gulp.task('copyDepends', function() {
	gulp.src('./node_modules/fancybox/dist/')
	.pipe(symlink(tplPath + 'app/dist', {force: true}));
});

gulp.task('compress-css', function() {
	gulp.src(tplPath + 'app/build.css')
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest(tplPath + 'app'));
});

gulp.task('compress-js', function() {
	gulp.src(tplPath + 'app/build.js')
	.pipe(uglify())
	.pipe(gulp.dest(tplPath + 'app'));
});

gulp.task('watch', ['jade', 'styles', 'server', 'scripts'], function() {
	watch(tplPath + 'jade/**/*.jade', function() {
		gulp.start('jade');
		browserSync.reload();
	});

	watch(tplPath + 'sass/**/*.sass', function() {
		gulp.start('styles');
	});

	watch(tplPath + 'scripts/**/*.js', function() {
		gulp.start('scripts');
	});
});

gulp.task('default', ['copyDepends', 'jade', 'styles', 'scripts']);

gulp.task('build-production', ['compress-css', 'compress-js']);
